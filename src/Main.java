import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        bai01();
//        bai02();
//        bai03();
//        bai04();
        bai05();
    }

    static void bai01() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập vào số x = ");
        int x = scanner.nextInt();
        System.out.print("Nhập vào số y = ");
        int y = scanner.nextInt();
        System.out.println("Tổng của x + y = " + (x + y));
    }

    static void bai02() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập vào số bị chia: ");
        int soBiChia = scanner.nextInt();
        System.out.print("Nhập vào số chia: ");
        int soChia = scanner.nextInt();

        int phanNguyen = soBiChia / soChia;
        int phanDu = soBiChia % soChia;

        if (phanDu == 0) {
            System.out.println("Phép chia hết, được kết quả lả " + phanNguyen);
        } else {
//            System.out.println("Phép chia có dư, được kết quả là " + phanNguyen + " dư " + phanDu);
            System.out.printf("Phép chia có dư, được kết quả là %d dư %d\n", phanNguyen, phanDu);
        }
    }

    static void bai03() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập vào chuỗi kí tự: ");
        String text = scanner.nextLine();
        System.out.print("Nhập vào kí tự cần tìm kiếm: ");
        char c = scanner.nextLine().charAt(0);

        int index = text.indexOf(c);
        if (index == -1) {
            System.out.println("Không xuất hiện!");
        } else {
            System.out.println("Có xuất hiện tại vị trí " + index);
        }
    }

    static void bai04() {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Nhập vào số nguyên dương N = ");
            int n = scanner.nextInt();

            if (n <= 0) {
                System.out.println("Số bạn nhập không phải số nguyên dương");
                continue;
            }

            for (int i = 0; i <= n; i++) {
                if (kiemTraSoNguyenTo(i)) {
                    System.out.print(i + " ");
                }
            }
            break;
        }
    }

    static boolean kiemTraSoNguyenTo(int number) {
        if (number < 2) {
            return false;
        }

        int count = 0;
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                count++;
            }
        }

        if (count == 2) {
            return true;
        }

        return false;
    }

    static void bai05() {
        int[] A = {1, 5, 4, 3, 7, 9, 11, 15, 10};
        int[] B = {34, 2, 6, 4, 3, 5, 6, 1, 17};

        while (true) {
            System.out.println("1. InramànhìnhTổnggiátrịcácphầntửcủamảngA.\n" +
                    "2. InramànhìnhTrungbìnhcộngcácgiátrịcủamảngB.\n" +
                    "3. InramànhìnhsốlớnnhấttrongmảngA.\n" +
                    "4. InramànhìnhsốnhỏnhấttrongmảngB.\n" +
                    "5. InramànhìnhdanhsáchsốnguyêntốcótrongmảngA.\n" +
                    "6. SắpxếpcácphầntửcủamảngAtheothứtựtăngdần.\n" +
                    "7. SắpxếpcácphầntửcủamảngBtheothứtựgiảmdần.\n" +
                    "8. Thoát.");

            Scanner scanner = new Scanner(System.in);
            System.out.print("Nhập lựa chọn của bạn: ");
            int option = scanner.nextInt();

            switch (option) {
                case 1:
                    System.out.println("Tổng giá trị mảng A là " + tinhTongPhanTuMang(A));
                    break;
                case 2:
                    int total = tinhTongPhanTuMang(B);
                    float average = (float) total / B.length;
                    System.out.printf("Trung bình côn của mảng B là %.2f \n", average);
                    break;
                case 3:
                    System.out.println("Giá trị lớn nhất trong mảng A là " + timPhanTuLonNhat(A));
                    break;
                case 4:
                    System.out.println("Giá trị nhỏ nhất trong mảng B là " + timPhanTuNhoNhat(B));
                    break;
                case 5:
                    System.out.println("Danh sách số nguyên tố trong mảng A là:");
                    inRaDanhSachSoNguyenTo(A);
                    break;
                case 6:

                    break;
                case 7:

                    break;
                case 8:
                    break;
                default:
                    break;
            }
        }
    }

    static int tinhTongPhanTuMang(int[] array) {
        int total = 0;

        for (int i = 0; i < array.length; i++) {
            total += array[i];
        }

        return total;
    }

    static int timPhanTuNhoNhat(int[] array) {
        int min = 0;

        if (array.length != 0) {
            min = array[0];
        } else {
            return min;
        }

        for (int element : array) {
            if (min > element) {
                min = element;
            }
        }

        return min;
    }

    static int timPhanTuLonNhat(int[] array) {
        int max = 0;

        if (array.length != 0) {
            max = array[0];
        } else {
            return max;
        }

        for (int element : array) {
            if (max < element) {
                max = element;
            }
        }

        return max;
    }

    static void inRaDanhSachSoNguyenTo(int[] array) {
        boolean hasPrime = false;
        for (int element : array) {
            if (kiemTraSoNguyenTo(element)) {
                System.out.printf("%d ", element);
                hasPrime = true;
            }
        }
        if(!hasPrime) {
            System.out.print("Không tìm thấy số nguyên tố trong mảng!");
        }
        System.out.println();
    }
}